/**
 * Pour indiquer qu'une interface utilisee par le composant 1
 * correspond a l'implementation d'une interface du composant 2.
 */
Composant1.Interface -> Composant2.Interface;


/**
 * Il est egalement possible d'indiquer que le composant 2 va chercher
 * une interface aupres du composant 1 en inversant le sens de la
 * fleche.
 */
Composant1.Interface <- Composant2.Interface;

/**
 * Lorsque les deux interfaces a relier ont le meme identificateur
 * il est possible de ne donner que le nom d'une des deux.
 */
Composant1.Interface -> Composant2;
