/*   Fait clignoter la led d'un STM32    */
/* (a l'aide de la bibliotheque libopencm3) */
#include <libopencm3/cm3/common.h>
#include <libopencm3/cm3/f1/memorymap.h>
#include <libopencm3/cm3/f1/rcc.h>
#include <libopencm3/cm3/f1/gpio.h>

static void gpio_setup(void)
{
  rcc_clock_setup_in_hse_8mhz_out_24mhz();

  rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPCEN);

  gpio_set_mode(GPIOC
		, GPIO_MODE_OUTPUT_2_MHZ
		, GPIO_CNF_OUTPUT_PUSHPULL
		, GPIO8 | GPIO9);
}

int main(void)
{
  int i;
  gpio_setup();
  gpio_set(GPIOC, GPIO8);
  gpio_clear(GPIOC, GPIO9);

  while(1)
    {
      gpio_toggle(GPIOC, GPIO8);
      gpio_toggle(GPIOC, GPIO9);

      for(i =0; i < 800000; i++)
	{
	  __asm__("nop");
	}
    }

  return 0;
}
