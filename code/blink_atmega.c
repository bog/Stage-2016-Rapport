/* Fait clignoter la led d'un atmega32u4 */
#include <avr/io.h>
#define F_CPU 16000000UL

int main(void)
{
  DDRB |= 1<<PORTB5;
  PORTB |= 1<<PORTB5;

  while(1)
    {
      PORTB ^= 1<<PORTB5;
      _delay_ms(500);
    }

  return 0;
}
