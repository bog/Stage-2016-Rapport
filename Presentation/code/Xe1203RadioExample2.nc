/**
 * The medium is free, Xe1203Radio can talk.
 */
event void Xe1203MediumAccessControl.granted() {
  error_t err;
  if(message_to_send == NULL) { return; }
  atomic {
    call Packet.setPayloadLength(message_to_send, len_message_to_send);
  }

  err = call UartStream.send((uint8_t*) message_to_send, sizeof(message_t));

  if( err == FAIL ) {
    if( call BackoffTimer.isRunning() ) {
    }
    uart_sent_error = SUCCESS;
    signalSendDone();
  }
}
