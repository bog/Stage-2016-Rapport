/**
 * Implementation of the Medium Access Control layer
 * of the Xe1203.
 * @author bog
 * @date April 2016
 * @version 1
 */

module Xe1203MediumAccessControlP
{
  provides
    {
      interface Xe1203MediumAccessControl as Mac;
    }

  uses
    {
      interface Xe1203DetectMediumAccess as Detect;
      interface Timer<TMilli>;
      interface Random;
    }
}
implementation
{
  /* ... implementation ... */
}
