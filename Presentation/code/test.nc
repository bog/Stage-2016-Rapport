/**
 * Blink example
 *
 * @author bog
 * @date 2016
 * @version 1
 */

module BlinkC
{
  uses
    {
      interface Boot;
      interface Timer<TMilli>;
      interface Leds;
    }
}
implementation
{
  event void Boot.booted()
  {
    call Timer.startPeriodic(500);
  }

  event void Timer.fired()
  {
    call Leds.led0Toggle();
  }
}
