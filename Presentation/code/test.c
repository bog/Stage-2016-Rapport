/**
 * A simple C hello world.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv)
{
  // print a message
  printf("Hello world !\n");

  return 0;
}
