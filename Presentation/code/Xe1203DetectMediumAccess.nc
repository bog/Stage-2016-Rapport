/**
 * Listen to the medium and notify if a signal
 * is emitted from an external source.
 *
 * @author bog
 * @date April 2016
 * @version 1
 */

interface Xe1203DetectMediumAccess
{
  /**
   * Start medium listening in order to check if the medium is free or
   * not.
   */
  command void listenMedium();

  /**
   * Tell if the medium whether the is free or not.
   * @param err The status of the listening, SUCCESS if all goes well.
   * @param free TRUE if the medium is free, FALSE otherwise.
   */
  event void listenMediumDone(error_t err, bool isMediumFree);
}
