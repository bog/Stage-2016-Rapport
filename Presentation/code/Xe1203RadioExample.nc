/**
 * Make the radio send a data packet.
 */
void send(void) {
  if(message_to_send == NULL) { return; }

  setPacketFcf(message_to_send, XE1203_FCF_DATA);

  if( isPacketRequestingAck(message_to_send) ) {
    (getMetadata(message_to_send)->ack) &= ~(1<<XE1203_ACK_ACKED);
  }

  timeout_message = message_to_send;
  call TimeoutTimer.startOneShot(timeout_delay);
  call Xe1203MediumAccessControl.request();
}
