/**
 * Blink example
 *
 * @author bog
 * @date 2016
 * @version 1
 */

configuration BlinkAppC
{
  
}
implementation
{
  components MainC, BlinkC;
  BlinkC.Boot -> MainC;
  
  components new TimerMilliC();
  BlinkC.Timer -> TimerMilliC;

  components LedsC;
  BlinkC.Leds -> LedsC;
}
