implementation
{
  /* ... */

  event void Detect.listenMediumDone(error_t err, bool isMediumFree)
  {
    if(err == SUCCESS && isMediumFree == TRUE)
      {
	signal Mac.granted();
      }
    else
      {
	uint16_t random_time = getRandomInt16(RANDOM_MILLI_MIN
					      , RANDOM_MILLI_MAX + 1);
	state = MAC_WAITING;
	call Timer.startOneShot( random_time );
      }
  }

  command void Mac.request()
  {
    /* ... */
    call Detect.listenMedium();
    /* ... */
  }

  /* ... */
}
