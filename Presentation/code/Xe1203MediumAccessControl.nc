/**
 * Control the access to the medium using a
 * Listen Before Talk (LBT) approch.
 *
 * @author bog
 * @date April 2016
 * @version 1
 */

interface Xe1203MediumAccessControl
{
  /**
   * Request for the medium access in order to emit.
   */
  command void request();

  /**
   * Response to a MAC request.
   * Signaled when an emission is possible.
   */
  event void granted();
}
