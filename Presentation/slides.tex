\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{listings}

\setcounter{tocdepth}{1}
% code
\definecolor{nesc-comment}{RGB}{0, 64, 0}
\definecolor{nesc-keyword}{RGB}{0, 0, 128}
\definecolor{nesc-string}{RGB}{128, 0, 0}
\lstset{
  language={C}
  , basicstyle=\tiny
  , keywordstyle=\color{nesc-keyword}
  , commentstyle=\color{nesc-comment}
  , stringstyle=\color{nesc-string}
  , frame=single
  , showstringspaces=false
  , numbers=left
  , stepnumber=2
  , numberstyle=\tiny
  , otherkeywords = {
    module
    , uses
    , provides
    , implementation
    , event
    , call
    , interface
  }
}
\newcommand{\code}[1]{\lstinputlisting{code/#1}}
%%

\usetheme{Frankfurt}
\usecolortheme{dolphin}

\setbeamercovered{transparent=10}
\addtobeamertemplate{navigation symbols}{}{
  \usebeamerfont{footline}
  \usebeamercolor[fg]{footline}
  \hfill

  \insertframenumber/\inserttotalframenumber
}

\setbeamertemplate{items}[square]

\newcommand{\todo}[1]{{\color{red} {\tiny???}{\bf #1}{\tiny???}}}

\newcommand{\node}[0]{n{\oe}ud}
\newcommand{\nodes}[0]{\node{s}}

% <1:id> <2:title> <3:caption> <4:img> <5:scale>
\newcommand{\fig}[5]{
  \begin{figure}
    \includegraphics[scale=#5]{img/#4}
    \caption[#2]{#3}
    \label{figure:#1}
  \end{figure}
}

% <1:title> <2:img> <3:scale>
\newcommand{\img}[3]{
  \includegraphics[scale=#3]{img/#2}\\
  \noindent\rule{\textwidth}{0.05mm}\\
  #1
}

\newcommand{\senseor}[0]{SENSeOR}

\title{Présentation du stage de licence 3 informatique}
\subtitle{Implémentation d'un réseau de collecte}
\author{Bérenger OSSÉTÉ GOMBÉ}

\begin{document}
\maketitle

\section{Introduction}

\subsection{Contexte}

\begin{frame}{Université de Franche-Comté}
  \begin{block}{Contexte du stage}
    \begin{itemize}
    \item Stage dans le cadre d'une troisième année de licence en
      informatique à l'Université de Franche-Comté.
    \item Tuteur universitaire : Didier Teifreto
    \item Maître de stage : Jean-Michel Friedt

    \item Du 14 mars 2016 au 12 août 2016 (5 mois)
    \end{itemize}
  \end{block}
  \vfill
  \includegraphics[scale=0.5]{img/baniere_ufr_st.png}
\end{frame}

\begin{frame}{FEMTO-ST}
  \begin{block}{Franche-Comté Électronique Mécanique Thermique et Optique - Sciences et Technologies}
    \begin{itemize}
    \item Est une unité de recherche mixte.
    \item Possède un certain nombre de département dont le
      département temps-fréquence (TF).
    \end{itemize}
  \end{block}

    \begin{block}{Le département temps-fréquence}
      \begin{itemize}
      \item Est composé de trois équipes de recherches :
        \begin{itemize}
        \item Acousto-électronique et piézoélectricité (ACEPI)
        \item Composants et systèmes micro-acoustiques (CoSyMA)
        \item Ondes, horloges, métrologie et systèmes (OHMS)
        \end{itemize}
      \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{\senseor}
  \begin{block}{SENSeOR est une entreprise}
    \begin{itemize}
    \item Spécialisée dans la conception et la fabrication de composants.
    \item Ces composants ont pour principale caractéristique leur
      utilisation des ondes élastiques de surface
      (SAW \footnote{Pour \textit{Surface Acoustic Wave}.}).
    \item Conçoit et fabrique des capteurs passifs sans-fils.
      \begin{itemize}
      \item Qui sont interrogeable à distance.
      \item Ne nécessitant pas de batterie pour fonctionner.
      \end{itemize}
    \end{itemize}
  \end{block}
  \vfill
  \begin{center}
    \includegraphics[scale=0.5]{img/logo_senseor.png}
  \end{center}
\end{frame}

\section{Plan de la présentation}
\begin{frame}{Plan de la présentation}
  \tableofcontents
\end{frame}

\section{Réseau de collecte}
\subsection{Représentation}
\begin{frame}{Application}{Que voulons-nous mettre en place ?}
  \begin{block}{Collecter des données}
    Notre objectif est de mettre en place un réseau local sans-fil
    (WLAN) :
    \begin{itemize}
    \item Composé de systèmes embarqués.
    \item Permettant la collecte de données générées en son sein.
    \item Routé dynamiquement et adaptatif aux changements de topologie.
    \item Capable de cohabiter dans un même environnement avec
      d'autres réseaux du même type.
    \end{itemize}
  \end{block}

  \uncover<2-> {
    \begin{block}{Utilisations possibles}
      \begin{itemize}
      \item Surveillance d'environnements :
        \begin{itemize}
        \item Naturels (exemple : fontes des glaces).
        \item Industriels (exemple : maintenance prédictive).
        \end{itemize}
      \end{itemize}
    \end{block}
  }

\end{frame}

\begin{frame}{Graphe}{Comment pouvons-nous représenter le réseau sans-fil ?}
  \begin{block}{Représentation du WLAN comme un graphe}
    Le réseau sans-fil est conceptuellement un graphe orienté
    acyclique (DAG).
    \begin{itemize}
    \item Les sommets (ou \nodes) sont des systèmes embarqués.
    \item Les arêtes sont les liaisons entre systèmes embarqués.
    \end{itemize}
  \end{block}

  \begin{center}
    \img{Exemple de graphe orienté acyclique}{dag.png}{0.35}
  \end{center}

\end{frame}

\subsection{Descriptions}

\begin{frame}{Réseau de collecte}{Qu'est-ce qu'un réseau de collecte ?}
  \begin{block}{Objectif : collecter des données}
    Un réseau de collecte est un \textbf{réseau} ayant pour rôle
    d'\textbf{acheminer des données} à partir de quelques points vers
    un ou plusieurs points de réseau dit ``point de collecte''.
    \begin{enumerate}
    \item<1-> Un {\node} effectue une mesure.
    \item Les {\nodes} propagent la mesure vers un ou plusieurs {\nodes} considérés
      comme étant des \textbf{points de collecte}.
    \end{enumerate}
  \end{block}

  \uncover<2->{
    \begin{alert}{Précisions}
      \begin{itemize}
      \item Une donnée peut être acheminée vers plusieurs points de
        collecte : on ne s'intéresse simplement pas aux duplications.
      \item Le réseau ne choisit pas directement le ou les points de
        collecte vers le(s)quel(s) la donnée sera acheminée\footnote{Il
          s'agit d'un adressage \textit{anycast}.}.
      \end{itemize}
    \end{alert}
  }
\end{frame}

\subsection{Protocole CTP}

\begin{frame}{Présentation du protocole}{Pourquoi utiliser le protocole Collection Tree Protocol (CTP) ?}
  \begin{block}{Le protocole CTP}
    \begin{itemize}
    \item Permet de mettre en place un tel \textbf{réseau de
      collecte}.
    \item Permet de faire cohabiter plusieurs réseaux de collecte
      dans un même environnement.
    \end{itemize}
  \end{block}
\end{frame}
  
\begin{frame}{Présentation du protocole}{Pourquoi utiliser le protocole Collection Tree Protocol (CTP) ?}
  \begin{minipage}{0.45\textwidth}
    \begin{block}{Le protocole CTP}
      \begin{itemize}
      \item Possède quelques prérequis
        \begin{itemize}
        \item Adresse de diffusion.
        \item Système d'acquittements synchrones.
        \item Destinataire et son expéditeur.
        \item Estimation de la qualité des liens.
        \end{itemize}
      \end{itemize}
    \end{block}
  \end{minipage}\hspace{0.05\textwidth}
  \begin{minipage}{0.45\textwidth}
    \includegraphics[scale=0.55]{img/dag.png}
  \end{minipage}
\end{frame}

\begin{frame}{Quatre types de sommet}{Quels rôles jouent les différents sommets ?}
  \begin{block}{Différents sommets}
    Nous pouvons différencier quatre comportements distincts parmis les
    sommets ({\nodes}) du réseau :
    \begin{itemize}
    \item N{\oe}ud producteur (\textit{producer}).
    \item N{\oe}ud propagateur (\textit{in-network processor}).
    \item N{\oe}ud consomateur (\textit{consumer}).
    \item N{\oe}ud témoin (\textit{snooper}).
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{}
  \begin{center}
    \img{Représentation graphique d'un réseau de collecte}{nodes.png}{0.4}
  \end{center}
\end{frame}

\section{État du projet}
\begin{frame}{Avancement}{Quel est le travail déjà effectué sur le projet ?}
  \begin{block}{Étapes clefs}
    \centering
    \begin{tabular}{| l | c | l |}
      \hline
      Contexte & Date & Travail Effectué \\
      \hline\hline
      Christophe Droit  & 2010 & Développement matériel. \\
      \hline
      Gwenhael Goavec-Mérou & 2011 & Support stm32 dans TinyOS.\\
      \hline
      Karla Breshi & 2012 & Liaison point-à-point\footnote{Les liaisons entre n{\oe}uds connexes.}.\\
      \hline
    \end{tabular}
  \end{block}
\end{frame}

\begin{frame}{Objectifs}{Quels sont nos objectifs concernant le projet ?}

  \begin{block}{Stage L3}
    Continuation du projet dans le cadre d'un stage de troisième année
    de licence 3 en informatique.
  \end{block}
  \uncover<2-> {
    \begin{block}{Nous voulons}
      \begin{itemize}
      \item Valider la mise-à-jour de TinyOS sur STM32.
      \item Valider le bon fonctionnement du radio-modem.
      \item Ajouter une couche MAC.
      \item Démontrer l'existence des liaisons multi-sauts et de l'arbre
        de routage.
      \item Endormir et réveiller un n{\oe}ud au bon moment pour fournir un
        service de routage suffisant.
      \end{itemize}
    \end{block}
  }
\end{frame}

\section{Outils utilisés}
\begin{frame}{Nos outils}{Quels outils pour mettre en place un réseau de collecte ?}
  \begin{block}{Deux types d'outils}
    \begin{itemize}
    \item Matériels : \textbf{capteurs passifs} et \textbf{STM32}.
    \item Logiciels : \textbf{NesC}, \textbf{TinyOS} et différents protocoles tel que \textbf{CTP}.
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Matériel}
\begin{frame}{Aspect matériel}{Quelles électroniques utilisons-nous ?}
  \begin{block}{Les capteurs passifs}
    \begin{itemize}
    \item Mesurent une grandeur physique.
    \item Sont interrogeables à distance.
    \item Ne nécessitent pas d'être alimenté par une source
      d'énergie locale.
    \end{itemize}
  \end{block}

  \begin{block}{Les cartes STM32}
    \uncover<2-> {
      \begin{itemize}
      \item Sont fabriquées par \senseor{}.
      \item Embarquent un microcontrôleur STM32.
      \item Font une double utilisation du radio-modem Xe1203.
        \begin{itemize}
        \item Pour faire de la communication réseau.
        \item Pour interroger des capteurs passifs.
        \end{itemize}
      \end{itemize}
    }
  \end{block}
\end{frame}

\subsection{Logiciel}
\begin{frame}{Aspect logiciel}{Comment programmer notre électronique pour avoir le comportement attendu ?}
  \begin{block}{Le NesC}
    \begin{itemize}
    \item Est un dialecte du langage C.
    \item Est conçu pour les WLAN.
    \item Est un langage
      \begin{itemize}
      \item événementiel.
      \item orienté composant.
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Mécanismes du langage NesC}{Quels sont les spécificités du langage NesC ?}
  \img{Mécanismes du langage NesC}{compo_types.png}{0.65}
\end{frame}

\begin{frame}{TinyOS}{Qu'est ce que TinyOS ?}
  \begin{block}{TinyOS}
    \begin{itemize}
    \item<1-> Est programmée en NesC.
    \item Est une structure logicielle\footnote{\textit{Framework}.
      dans la langue de Shakespeare.}
    \item Est découpée en couche d'abstractions :
      \begin{enumerate}
      \item \textbf{HPL} : \textit{Hardware Presentation Layer}
      \item \textbf{HAL} : \textit{Hardware Adaptation Layer}
      \item \textbf{HIL} : \textit{Hardware Interface Layer}
      \end{enumerate}
    \item Permet d'écrire des programmes portables sur un ensemble
      de plateformes matérielles.
    \item Fournit un ensemble de mécanismes et d'algorithmes
      utilisables (\textbf{Dont une implémentation du protocole CTP}).
    \end{itemize}
  \end{block}
\end{frame}

\section{Résultats}
\begin{frame}{Contributions}{Quelles sont nos contributions au projet ?}
  \begin{block}{Contributions}
    \begin{itemize}
    \item<1> Détection de légers dysfonctionnements dû au changement de
      version de TinyOS.
    \item<1> Réparation du pilote du radio-modem (12 transmissions maximum).
    \item<2-> Ajout d'une couche MAC.
    \item<2-> Ajout du système d'acquittement manquant au pilote du radio-modem.
    \item<2-> Constat du fonctionnement de l'implémentation du
      protocole CTP pour le STM32.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Ajout de la couche MAC}{Comment fonctionne la couche MAC ?}
  \begin{center}
    \includegraphics[scale=0.27]{img/algo_mac.png}
  \end{center}
\end{frame}

\begin{frame}{Ajout du système d'acquittement}{Pourquoi voulions-nous un système d'acquittement ?}
  \begin{block}{Un système d'acquittement}
    \begin{itemize}
    \item Permet de détecter les erreurs de transmissions.
    \item Permet le calcul de l'évaluation d'un lien entre deux n{\oe}uds.
    \item Est un prérequis au protocole CTP.
    \end{itemize}
  \end{block}
  \begin{center}
    \includegraphics[scale=0.47]{img/exp_ack.png}
  \end{center}
\end{frame}

\begin{frame}{Visualisation de l'arbre de routage}{Comment constater que l'arbre de routage se forme bien ?}
  \begin{block}{Mise en place d'un montage}
    \begin{itemize}
    \item Les différents n{\oe}uds propagent des paquets vers un
      n{\oe}ud collecteur.
    \item Chaque n{\oe}ud indique dans les paquets envoyés l'adresse de
      son parent.
    \item Le n{\oe}ud collecteur
      \begin{itemize}
      \item connait le n{\oe}ud parent des autres n{\oe}uds.
      \item communique ces informations à l'ordinateur auquel il est
        connecté par USB.
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Visualisation de l'arbre de routage}{Comment constater que l'arbre de routage se forme bien ?}
  \begin{block}{Un programme tiers}
    \begin{itemize}
    \item utilise ces données pour former un arbre.
    \item exporte une image de l'arbre de routage à l'aide du
      programme {\sf dot}.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Visualisation de l'arbre de routage}{Comment constater que l'arbre de routage se forme bien ?}
  \begin{center}
    \includegraphics[scale=0.1]{img/treedot_real.jpg}
  \end{center}
\end{frame}

\begin{frame}{Visualisation de l'arbre de routage}{Comment constater que l'arbre de routage se forme bien ?}
  \begin{center}
    \includegraphics[scale=0.4]{img/treedot.png}
  \end{center}
\end{frame}

\section{Conclusion}
\subsection{Travail restant}
\begin{frame}{Travail restant}{Que manque t-il pour que le réseau de collecte soit complet ?}
  \begin{block}{Travail restant}
    Fonctionnalité souhaitée pour avoir un réseau de collecte complet :
    \begin{itemize}
    \item Endormissement des {\nodes} du réseau et réveil aux bons
      moments pour fournir un service de routage.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Bilan}{Que retenir de la présentation ?}
  \begin{block}{Ce que l'on a vu}
    \begin{itemize}
    \item Utilisation de capteurs passifs pour effectuer des mesures.
    \item Mise en place d'un graphe dont les n{\oe}uds sont des STM32 pour propager les mesures vers des points de
      collecte.
    \item Utilisation de la structure logicielle TinyOS en langage NesC
      \uncover<2-> {
        \begin{itemize}
        \item Qui est évenementiel.
        \item Qui est orienté composant.
        \item Qui nous permet d'écrire des programmes
          portables\footnote{Sur un ensemble de plateformes supportées.}.
        \item Qui fournit des modules pour nos programmes comme une
          implémentation de CTP.
        \end{itemize}
      }
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Fin de la présentation}{Merci pour votre attention.}
  \tableofcontents
\end{frame}

\end{document}
