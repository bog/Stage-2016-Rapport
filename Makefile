BASENAME=report
TEMP=$(wildcard *.aux *.bbl *.blg *.glg *.glo *.gls *.ist *.log *.out *.toc *.lof *.lot *.acr *.alg *.acn)

all:pdf

pdf:$(BASENAME).tex findref
	pdflatex $<

findref:$(BASENAME).tex bib glossaries
	pdflatex $<

bib:prepare
	bibtex $(BASENAME).aux

glossaries:prepare
	makeglossaries $(BASENAME)

prepare:$(BASENAME).tex
	pdflatex $<

clean:
	rm -f $(TEMP)

mrproper:clean
	rm -f $(BASENAME).pdf
